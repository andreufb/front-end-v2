import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { DropdownQuestion } from './question-dropdown';
import { TextboxQuestion } from './question-textbox';
let QuestionService = class QuestionService {
    // TODO: get from a remote source of question metadata
    getData() {
        let questions = [
            new DropdownQuestion({
                key: 'endpoint',
                label: 'Type of data',
                options: [
                    { key: 'physical-system', value: 'Physical system' },
                    { key: 'person', value: 'Person' },
                    { key: 'project', value: 'Project' },
                    { key: 'mission', value: 'Mission' },
                    { key: 'action', value: 'Action' },
                    { key: 'location', value: 'Location' },
                    { key: 'feature-of-interest', value: 'Feature of interest' },
                    { key: 'observable-property', value: 'Observable property' },
                    { key: 'unit-of-measurement', value: 'Unit of measurement' },
                    { key: 'observation', value: 'Observation' }
                ],
                order: 1,
                hint: '',
            }),
            new TextboxQuestion({
                key: 'id',
                label: 'ID',
                order: 2
            }),
        ];
        return questions.sort((a, b) => a.order - b.order);
    }
    ;
    getPhysicalSystemQuestions() {
        let questions = [
            new TextboxQuestion({
                key: 'identifiers',
                label: 'Identifyers',
                required: true,
                order: 1,
                endpoint: 'physical-system',
                method: 'PUT'
            }),
            new TextboxQuestion({
                key: 'contacts',
                label: 'Contacts',
                required: true,
                order: 2
            }),
            new TextboxQuestion({
                key: 'observableProperties',
                label: 'Observable properties',
                required: true,
                order: 3
            }),
            new TextboxQuestion({
                key: 'attachedTo',
                label: 'Attached to:',
                required: true,
                order: 4
            })
        ];
        return questions.sort((a, b) => a.order - b.order);
    }
    ;
    getPersonQuestions() {
        let questions = [
            new TextboxQuestion({
                key: 'name',
                label: 'Name',
                required: true,
                order: 1,
                endpoint: 'person',
                method: 'PUT'
            }),
            new TextboxQuestion({
                key: 'mail',
                label: 'Mail',
                required: true,
                order: 1
            }),
            new TextboxQuestion({
                key: 'role',
                label: 'Role',
                required: true,
                order: 1
            })
        ];
        return questions.sort((a, b) => a.order - b.order);
    }
    ;
    getMissionQuestions() {
        let questions = [
            new TextboxQuestion({
                key: 'label',
                label: 'Label',
                required: true,
                order: 1,
                endpoint: 'mission',
                method: 'PUT'
            }),
            new TextboxQuestion({
                key: 'description',
                label: 'Description',
                required: true,
                order: 2
            })
        ];
        return questions.sort((a, b) => a.order - b.order);
    }
    ;
};
QuestionService = __decorate([
    Injectable()
], QuestionService);
export { QuestionService };
//# sourceMappingURL=question.service.js.map