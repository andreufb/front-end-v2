import { QuestionBase } from './question-base';
export class DropdownQuestion extends QuestionBase {
    constructor(options = {}) {
        super(options);
        this.controlType = 'dropdown';
        this.options = [];
        this.options = options['options'] || [];
    }
}
//# sourceMappingURL=question-dropdown.js.map