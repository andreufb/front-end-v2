import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { QuestionControlService } from './question-control.service';
let DynamicFormComponent = class DynamicFormComponent {
    constructor(qcs, _snackBar) {
        this.qcs = qcs;
        this._snackBar = _snackBar;
        this.questions = [];
        this.close = new EventEmitter();
        this.submit = new EventEmitter();
        this.payLoad = '';
        this.endpoint = '';
        this.method = '';
    }
    ngOnInit() {
        this.form = this.qcs.toFormGroup(this.questions);
    }
    onSubmit() {
        this._snackBar.open(`Submited Successfully`, '', {
            duration: 4000,
            panelClass: 'snackbar-success',
            verticalPosition: 'top',
            horizontalPosition: 'center'
        }).afterOpened().subscribe(() => {
            this.submit.emit(this.form.value);
            this.payLoad = JSON.stringify(this.form.value);
        });
    }
    onCancel() {
        this.close.emit(null);
    }
};
__decorate([
    Input()
], DynamicFormComponent.prototype, "questions", void 0);
__decorate([
    Input()
], DynamicFormComponent.prototype, "isDialog", void 0);
__decorate([
    Output()
], DynamicFormComponent.prototype, "close", void 0);
__decorate([
    Output()
], DynamicFormComponent.prototype, "submit", void 0);
DynamicFormComponent = __decorate([
    Component({
        selector: 'app-dynamic-form',
        templateUrl: './dynamic-form.component.html',
        styleUrls: ['./dynamic-form.component.css'],
        providers: [QuestionControlService]
    })
], DynamicFormComponent);
export { DynamicFormComponent };
//# sourceMappingURL=dynamic-form.component.js.map