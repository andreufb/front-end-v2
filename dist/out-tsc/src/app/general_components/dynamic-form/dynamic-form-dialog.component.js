import { __decorate, __param } from "tslib";
import { Component, Input, Inject } from '@angular/core';
import { QuestionControlService } from './question-control.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
let DynamicFormDialogComponent = class DynamicFormDialogComponent {
    constructor(qcs, _snackBar, dialogRef, data) {
        this.qcs = qcs;
        this._snackBar = _snackBar;
        this.dialogRef = dialogRef;
        this.data = data;
        this.questions = [];
        this.questions = this.data.questions;
    }
    ngOnInit() {
    }
    closeDialog() {
        this.dialogRef.close();
    }
};
__decorate([
    Input()
], DynamicFormDialogComponent.prototype, "questions", void 0);
DynamicFormDialogComponent = __decorate([
    Component({
        selector: 'app-dynamic-form-dialog',
        templateUrl: './dynamic-form-dialog.component.html',
        providers: [QuestionControlService]
    }),
    __param(3, Inject(MAT_DIALOG_DATA))
], DynamicFormDialogComponent);
export { DynamicFormDialogComponent };
//# sourceMappingURL=dynamic-form-dialog.component.js.map