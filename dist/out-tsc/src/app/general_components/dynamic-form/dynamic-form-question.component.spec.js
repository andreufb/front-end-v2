import { async, TestBed } from '@angular/core/testing';
import { DynamicFormQuestionComponent } from './dynamic-form-question.component';
describe('DynamicFormQuestionComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DynamicFormQuestionComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(DynamicFormQuestionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=dynamic-form-question.component.spec.js.map