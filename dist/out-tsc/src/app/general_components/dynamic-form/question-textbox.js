import { QuestionBase } from './question-base';
export class TextboxQuestion extends QuestionBase {
    constructor(options = {}) {
        super(options);
        this.controlType = 'textbox';
        this.type = options['type'] || '';
    }
}
//# sourceMappingURL=question-textbox.js.map