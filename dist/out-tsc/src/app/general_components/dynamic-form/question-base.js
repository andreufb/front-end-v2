export class QuestionBase {
    constructor(options = {}) {
        this.value = options.value;
        this.key = options.key || '';
        this.label = options.label || '';
        this.required = !!options.required;
        this.order = options.order === undefined ? 1 : options.order;
        this.controlType = options.controlType || '';
        this.type = options.type || '';
        this.endpoint = options.endpoint;
        this.method = options.method;
    }
}
//# sourceMappingURL=question-base.js.map