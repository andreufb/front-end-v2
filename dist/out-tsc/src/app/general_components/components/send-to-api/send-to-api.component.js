import { __decorate } from "tslib";
import { Component } from '@angular/core';
let SendToApiComponent = class SendToApiComponent {
    constructor(selectedEndpointService) {
        this.selectedEndpointService = selectedEndpointService;
    }
    ngOnInit() {
        this.endpointsData = this.selectedEndpointService.getEndpointsData();
    }
};
SendToApiComponent = __decorate([
    Component({
        selector: 'app-send-to-api',
        templateUrl: './send-to-api.component.html',
        styleUrls: ['./send-to-api.component.css']
    })
], SendToApiComponent);
export { SendToApiComponent };
//# sourceMappingURL=send-to-api.component.js.map