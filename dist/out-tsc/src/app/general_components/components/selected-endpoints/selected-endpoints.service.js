import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
let SelectedEndpointsService = class SelectedEndpointsService {
    constructor(http) {
        this.http = http;
        this.endpoints = [];
    }
    addToSelected(endpoint) {
        this.endpoints.push(endpoint);
    }
    ;
    getEndpoints() {
        return this.endpoints;
    }
    ;
    clearSelected() {
        this.endpoints = [];
        return this.endpoints;
    }
    ;
    getEndpointsData() {
        return this.http.get('http://147.83.159.160:5000/api/mission/');
    }
};
SelectedEndpointsService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], SelectedEndpointsService);
export { SelectedEndpointsService };
//# sourceMappingURL=selected-endpoints.service.js.map