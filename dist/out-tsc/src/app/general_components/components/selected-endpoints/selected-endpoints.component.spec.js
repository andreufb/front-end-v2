import { async, TestBed } from '@angular/core/testing';
import { SelectedEndpointsComponent } from './selected-endpoints.component';
describe('SelectedEndpointsComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SelectedEndpointsComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(SelectedEndpointsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=selected-endpoints.component.spec.js.map