import { __decorate } from "tslib";
import { LoginGuard } from './general_components/components/login/login.guard';
import { LoginComponent } from './general_components/components/login/login.component';
import { SendToApiComponent } from './general_components/components/send-to-api/send-to-api.component';
import { SelectedEndpointsComponent } from './general_components/components/selected-endpoints/selected-endpoints.component';
import { EndpointDetailsComponent } from './general_components/components/endpoints_details/endpoint-details.component';
import { OptionsComponent } from './general_components/components/options/options.component';
import { GetDataComponent } from './general_components/components/get-data/get-data.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
const routes = [];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forRoot([
                { path: '', component: OptionsComponent },
                { path: 'getData', component: GetDataComponent, canActivate: [LoginGuard] },
                { path: 'putData', component: SendToApiComponent },
                { path: 'updateData', component: SendToApiComponent },
                { path: 'login', component: LoginComponent },
                { path: 'endpoints/:endpointName', component: EndpointDetailsComponent },
                { path: 'selected', component: SelectedEndpointsComponent }
            ])
        ],
        exports: [RouterModule]
    })
], AppRoutingModule);
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map