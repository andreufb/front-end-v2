import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  images: any[] = [
    {
      name: "SARTI's A team",
      img: '../../../assets/images/SARTI_1.jpeg',
      desc: 'People working at SARTI are specialized in differnt areas'
    },
    {
      name: 'OBSEA',
      img: '../../../assets/images/SARTI_2.JPG',
      desc: 'Acording to us, one of the most valuable sea floor expandable observatories in the world!'
    },
    {
      name: 'SARTI divers at OBSEA',
      img: '../../../assets/images/SARTI_3.jpg',
      desc: "OBSEA gives SARTI's people a lot of work due to the harsh conditions under the sea"
    },
    {
      name: "SARTI's Meteorological buoy",
      img: '../../../assets/images/SARTI_4.JPG',
      desc: 'This buoy near the OBSEA gives a lot of information about the conditions above the sea'
    },
    {
      name: 'AUV',
      img: '../../../assets/images/SARTI_6.jpg',
      desc: "Gives a lot of information about currents when used, sadly it's not used a lot"
    }
  ]

  constructor(private config: NgbCarouselConfig) { }

  ngOnInit(): void {
  }

}
