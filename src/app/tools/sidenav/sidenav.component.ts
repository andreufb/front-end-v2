import { ButtonSettings } from '../button/button-settings';
import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  @Input() parent: string;
  @Input() buttonSettings: Array<ButtonSettings>;

  constructor() { }

  ngOnInit(): void {
  }

}