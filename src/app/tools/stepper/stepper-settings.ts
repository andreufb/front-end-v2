import { FormGroup } from '@angular/forms';

export class StepperSettings {
  label: string;
  form_name: any;
  add_more: boolean;
  order: number;
  steps: Array<StepSettings>;
  constructor(options: {
    label?: string,
    form_name?: any,
    add_more?: boolean,
    order?: number,
    steps?: any,
  } = {}) {
    this.label = options.label || '';
    this.form_name = options.form_name;
    this.add_more = options.add_more || false;
    this.order = options.order;
    this.steps = options.steps;
  }
}

class StepSettings {
  key: any;
  label: string;
  type: string;
  placeHolder: string;
  value: any;
  required: boolean;
  constructor(options: {
    key?: any;
    label?: string;
    type?: string;
    placeHolder?: string;
    value?: any;
    required?: boolean;
  } = {}) {
    this.key = options.key;
    this.label = options.label;
    this.type = options.type;
    this.placeHolder = options.placeHolder;
    this.value = options.value;
    this.required = options.required;
  }
}