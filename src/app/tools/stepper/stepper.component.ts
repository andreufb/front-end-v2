import { MatSnackBar } from '@angular/material/snack-bar';
import { StepperSettings } from './stepper-settings';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css']
})
export class StepperComponent implements OnInit {

  @Input() stepperSettings: Array<StepperSettings>;

  @Output() submit = new EventEmitter<any>();
  @Output() submit_list = new EventEmitter<any>();
  @Output() reset = new EventEmitter<any>();
  @Output() send = new EventEmitter<any>();
  @Output() download = new EventEmitter<any>();
  @Output() add = new EventEmitter<any>();

  constructor(private _snackBar: MatSnackBar, private _formBuilder: FormBuilder) { }

  len;
  list_values = []

  ngOnInit() {
    this.len = this.stepperSettings.length

    this.stepperSettings.forEach(function (step) {
      var group = {};
      step.steps.forEach(function (val) {
        group[val.key] = new FormControl(val.value);
      })
      step.form_name = new FormGroup(group);
    });
  }

  onSubmit(form, order) {
    form.order = order
    this.submit.emit(form);
  }

  add_val(form) {
    this.list_values.push(form.value)
  }

  del_val(index) {
    this.list_values.splice(index, 1)
  }

  send_list(order) {
    this.submit_list.emit({values: this.list_values, order: order})
  }

  functionality(value) {
    if (value == 'reset') {
      this.reset.emit()
    }
    else if (value == 'send') {
      this.send.emit()
    }
    else {
      this.download.emit()
    }
  }

  objectKeys = Object.keys;

}
