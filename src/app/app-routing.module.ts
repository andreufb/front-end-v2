import { ConfigureComponent } from './sections/home/data/configure/configure.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SensorComponent } from './sections/home/data/configure/sensor/sensor.component';
import { HomeComponent } from './sections/home/home.component';
import { UpdateDataComponent } from './sections/home/data/update-data/update-data.component'; import { LoginGuard } from './sections/login/login.guard';
import { LoginComponent } from './sections/login/login.component';

import { DataComponent } from './sections/home/data/data.component';
import { GetDataComponent } from './sections/home/data/get-data/get-data.component';
import { PutDataComponent } from './sections/home/data/put-data/put-data.component';
import { AccountsComponent } from './sections/home/accounts/accounts.component';
import { QualityManualComponent } from './sections/home/quality-manual/quality-manual.component';



const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    { path: '',   redirectTo: 'home', pathMatch: 'full' },
    // { path: '**',   redirectTo: 'home'},  //When cant find route go to home
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomeComponent, canActivate: [LoginGuard], children: [
      { path: 'data', component: DataComponent, canActivate: [LoginGuard],  children: [
        { path: 'getData', component: GetDataComponent, canActivate: [LoginGuard], outlet: 'sidenav' },
        { path: 'putData', component: PutDataComponent, canActivate: [LoginGuard], outlet: 'sidenav' },
        { path: 'updateData', component: UpdateDataComponent, canActivate: [LoginGuard], outlet: 'sidenav' },
        { path: 'configure', component: ConfigureComponent, canActivate: [LoginGuard], outlet: 'sidenav' }
      ]},
      { path: 'accounts', component: AccountsComponent, canActivate: [LoginGuard]},
      { path: 'quality_manual', component: QualityManualComponent, canActivate: [LoginGuard] }   
    ]}
  ])
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
