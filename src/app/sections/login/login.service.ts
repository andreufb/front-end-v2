import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Router } from '@angular/router';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

interface Response {
  token: string;
}


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  response;

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router,
  ) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  get isLoggedIn() {
    if (localStorage.getItem('loggedIn') == "true") {
      return true
    }
    return false;
  }

  get actualToken() {
    return localStorage.getItem('token');
  }


  getToken(data) {
    this.http.post('http://147.83.140.4:5000/api/auth/', JSON.stringify(data), this.httpOptions).pipe(retry(1), catchError(this.errorHandl)).subscribe(
      data => {
        this.response = JSON.parse(JSON.stringify(data));
        console.log("Token created")
        if (this.response.token.length > 5) {

          localStorage.setItem('loggedIn', 'true');
          localStorage.setItem('token', this.response.token.toString());

          this.snackBar.open(`Access granted!`, 'Now you can use the other methods', {
            duration: 6000,
            panelClass: 'snackbar-success',
            verticalPosition: 'top',
            horizontalPosition: 'center'
          });
          this.router.navigate(['/home'])
        };
      });
  };
  
  errorHandl(error) {
    let errorMessage = 'Email or password incorrect';
    localStorage.setItem('loggedIn', 'false');
    localStorage.setItem('token', 'null');
    // if (error.error instanceof ErrorEvent) {
    //   // Get client-side error
    //   errorMessage = error.error.message;
    // } else {
    //   // Get server-side error
    //   errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    // }
    console.log(errorMessage);
    this.snackBar.open(`Acces not granted`, 'User or passwrod incorrect', {
      duration: 6000,
      panelClass: 'snackbar-success',
      verticalPosition: 'top',
      horizontalPosition: 'center'
    });
    return throwError(errorMessage);
  };

}