import { ButtonSettings } from './../../tools/button/button-settings';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm;
  has_token: boolean = false;

  settings: ButtonSettings =  {
  'disabled': false,
  'label': "Submit",
  'type': "next"
}


  constructor(
    private loginService: LoginService,
    private formBuilder: FormBuilder,
    private router: Router

  ) {
    this.loginForm = this.formBuilder.group({
      email: '',
      password: ''
    });
  };

  ngOnInit(): void {
    this.has_token = this.loginService.isLoggedIn
    if (this.has_token == true) {
      setTimeout(() => {
        this.router.navigate(['/home'])       
      }, 1000)
      
    }
  };

  onSubmit(customerData) {
    this.loginService.getToken(customerData);
    this.loginForm.reset();
  };
  







}