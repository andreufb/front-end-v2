import { ButtonSettings } from './../../tools/button/button-settings';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private loginService: LoginService, public router: Router) {  }

  has_token = false;

  back_button: ButtonSettings = {
    'disabled': false,
    'label': "Go back",
    'type': "sidenav"
  }

  buttonSettings: Array<ButtonSettings> = [{
    'disabled': false,
    'label': "Data",
    'type': "sidenav",
    'route': 'data'
  },
  {
    'disabled': false,
    'label': "Accounts",
    'type': "sidenav",
    'route': 'accounts'
  },
  {
    'disabled': false,
    'label': "Quality manual",
    'type': "sidenav",
    'route': 'quality_manual'
  },
  ];


  ngOnInit(): void {
    this.has_token = this.loginService.isLoggedIn
  }

}
