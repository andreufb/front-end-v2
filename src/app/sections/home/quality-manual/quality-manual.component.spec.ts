import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QualityManualComponent } from './quality-manual.component';

describe('QualityManualComponent', () => {
  let component: QualityManualComponent;
  let fixture: ComponentFixture<QualityManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QualityManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QualityManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
