import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { StepperSettings } from './../../../../../tools/stepper/stepper-settings';


@Injectable({
  providedIn: 'root'
})

export class SensorService {

  sensor_config: Array<StepperSettings>;
  response;

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router,
  ) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };


  async getSensorConfig() {
    await new Promise((res) => {
      this.http.get('http://147.83.140.4:5000/api/config_sensor/', this.httpOptions).pipe(retry(1), catchError(this.errorHandl)).subscribe(
        data => {
          this.sensor_config = JSON.parse(JSON.stringify(data));
          res()
        });
    });
    return this.sensor_config
  };

  async putSensorConfig(data) {
    await new Promise((res) => {
      this.http.put('http://147.83.140.4:5000/api/config_sensor/', JSON.stringify(data), this.httpOptions).pipe(retry(1), catchError(this.errorHandl)).subscribe(
      data => {
        console.log(data)
        this.response = data
        res()
      });
    });
    return this.response  
  };

  errorHandl(error) {
    let errorMessage = 'Cant get sensor configuration';
    console.log(errorMessage);
    this.snackBar.open(`Cant laod stepper`, 'Cant get sensor configuration', {
      duration: 6000,
      panelClass: 'snackbar-success',
      verticalPosition: 'top',
      horizontalPosition: 'center'
    });
    return throwError(errorMessage);
  };

}