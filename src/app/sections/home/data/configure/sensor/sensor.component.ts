import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit } from '@angular/core';
import { SensorService } from './sensor.service';


@Component({
  selector: 'app-sensor',
  templateUrl: './sensor.component.html',
  styleUrls: ['./sensor.component.css']
})
export class SensorComponent implements OnInit {

  constructor(private sensorService: SensorService, private snackBar: MatSnackBar,) { }

  stepperSettings;
  response;

  get_config() {
    this.sensorService.getSensorConfig().then(data => {
      this.stepperSettings = data;
    })
    return null
  }

  ngOnInit() {
    this.get_config().then()
  }

  payload = [];

  values(eventValue) {
    this.payload[eventValue.order - 1] = eventValue.value
  }

  list_values(eventValue) {
    this.payload[eventValue.order - 1] = eventValue.values;
  }

  reset() {
    this.payload.length = 0
  }

  async send() { 
    await this.sensorService.putSensorConfig(this.payload).then(data => {
      this.response = data
    })
    
    await this.snackBar.open(JSON.stringify(this.response), '', {
      duration: 6000,
      panelClass: 'snackbar-success',
      verticalPosition: 'top',
      horizontalPosition: 'center'
    });

    
  
  }

  download() {
    // @ts-ignore Ignoro la proxima linea de codi perque marca error al no trobar shortName (es defineix a l'stepper)
    var download_name = 'config_person.json';
    var json = [JSON.stringify(this.payload)];
    var blob = new Blob(json, { type: "application/json;charset=urf-8" });
    var isIE = false
    if (isIE) {
      window.navigator.msSaveBlob(blob, download_name);
    } else {
      var url = window.URL || window.webkitURL;
      var link = url.createObjectURL(blob);
      var a = document.createElement("a");
      a.download = download_name;
      a.href = link;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
  }
}



