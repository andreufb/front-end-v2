import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';


const baseURL = 'http://147.83.140.4:5000/api/';

@Injectable({
  providedIn: 'root'
})
export class GetDataService {

  response = [];
  endpoint;
  id;
  url

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router,
  ) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };


  async getData(data) {
    this.endpoint = baseURL + data.endpoint;
    this.id = data.id
    this.url = this.endpoint + '/' + this.id;
    if (this.id == null) {
      this.url = this.endpoint + '/';
    }
    await new Promise ((res) => {
      this.http.get(this.url, this.httpOptions).pipe(retry(1), catchError(this.errorHandl)).subscribe(
      data => {
        this.response.push(JSON.parse(JSON.stringify(data)));
        res()
      });
    });
    return this.response[this.response.length - 1]

  };

  getAllData() {
    return this.response
  }

  errorHandl(error) {
    let errorMessage = 'Email or password incorrect';
    localStorage.setItem('loggedIn', 'false');
    localStorage.setItem('token', 'null');
    console.log(errorMessage);
    this.snackBar.open(`Acces not granted`, 'User or passwrod incorrect', {
      duration: 6000,
      panelClass: 'snackbar-success',
      verticalPosition: 'top',
      horizontalPosition: 'center'
    });
    return throwError(errorMessage);
  };

}