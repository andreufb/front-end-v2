import { QuestionService } from './tools/dynamic-form/question.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Material
import { MatSliderModule } from '@angular/material/slider';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatDialogModule } from '@angular/material/dialog';
import { A11yModule } from '@angular/cdk/a11y';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PortalModule } from '@angular/cdk/portal';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import 'hammerjs';

// Components
import { ToolBarComponent } from './tools/tool-bar/tool-bar.component';
import { FooterComponent } from './tools/footer/footer.component';
import { SliderComponent } from './tools/slider/slider.component';
import { ButtonComponent } from './tools/button/button.component';
import { SidenavComponent } from './tools/sidenav/sidenav.component';
import { LoginComponent } from './sections/login/login.component';
import { HomeComponent } from './sections/home/home.component';
import { DataComponent } from './sections/home/data/data.component';
import { GetDataComponent } from './sections/home/data/get-data/get-data.component';
import { PutDataComponent } from './sections/home/data/put-data/put-data.component';
import { UpdateDataComponent } from './sections/home/data/update-data/update-data.component';
import { ConfigureComponent } from './sections/home/data/configure/configure.component';
import { SensorComponent } from './sections/home/data/configure/sensor/sensor.component';
import { QualityManualComponent } from './sections/home/quality-manual/quality-manual.component';
import { AccountsComponent } from './sections/home/accounts/accounts.component';


// Services
import { LoginService } from './sections/login/login.service';

import { LoginGuard } from './sections/login/login.guard';
import { GetDataService } from './sections/home/data/get-data/get-data.service';
import { PutDataService } from './sections/home/data/put-data/put-data.service';
import { DynamicFormQuestionComponent } from './tools/dynamic-form/dynamic-form-question.component';
import { DynamicFormDialogComponent } from './tools/dynamic-form/dynamic-form-dialog.component';
import { DynamicFormComponent } from './tools/dynamic-form/dynamic-form.component';
import { StepperComponent } from './tools/stepper/stepper.component';
import { PersonComponent } from './sections/home/data/configure/person/person.component';


@NgModule({
  declarations: [
    AppComponent,
    ToolBarComponent,
    FooterComponent,
    GetDataComponent,
    DataComponent,
    LoginComponent,
    SliderComponent,
    DynamicFormQuestionComponent,
    DynamicFormDialogComponent,
    DynamicFormComponent,
    PutDataComponent,
    UpdateDataComponent,
    HomeComponent,
    SensorComponent,
    ButtonComponent,
    SidenavComponent,
    QualityManualComponent,
    AccountsComponent,
    ConfigureComponent,
    StepperComponent,
    PersonComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatSliderModule,
    MatButtonModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTabsModule,
    MatSnackBarModule,
    MatRadioModule,
    MatDialogModule,
    BrowserAnimationsModule,
    A11yModule,
    ClipboardModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    PortalModule,
    ScrollingModule
  ],

  providers: [
    LoginService,
    LoginGuard,
    GetDataService,
    PutDataService,
    QuestionService],

  bootstrap: [AppComponent],

  entryComponents: [DynamicFormDialogComponent]
})
export class AppModule { }
